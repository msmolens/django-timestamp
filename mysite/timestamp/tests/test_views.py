import jsonschema

from django.test import TestCase
from django.urls import reverse


# JSON Schema for response from the timestamp endpoint.
# See https://json-schema.org/understanding-json-schema/index.html.
SCHEMA = {
    '$schema': 'http://json-schema.org/draft-07/schema#',
    'type': 'object',

    # Require 'timestamp' field
    'required': ['timestamp'],

    'properties': {
        # Timestamp
        'timestamp': {
            'type': 'string',
            # Require date/time in ISO 8601 format
            # See https://json-schema.org/understanding-json-schema/reference/string.html#built-in-formats  # noqa: E501
            'format': 'date-time'
        }
    },

    # Disallow extra properties
    'additionalProperties': False
}


class TimestampViewTests(TestCase):
    """
    Tests for the timestamp view.
    """
    def test_get_timestamp(self):
        """
        Test that a GET request to the timestamp endpoint responds with a JSON
        object containing the current time in UTC.
        """
        response = self.client.get(reverse('timestamp:index'))
        self.assertEqual(response.status_code, 200)

        # Get the body of the response, parsed as JSON
        data = response.json()

        # Check that response confirms to schema, including the 'date-time' format
        # https://python-jsonschema.readthedocs.io/en/latest/validate/#validating-formats
        jsonschema.validate(data, SCHEMA, format_checker=jsonschema.FormatChecker())

        # TODO: Could parse timestamp and compare with the current time in UTC
        # on the local machine

    def test_head_timestamp(self):
        """
        Test that a HEAD request to the timestamp endpoint is successful.
        """
        response = self.client.head(reverse('timestamp:index'))

        self.assertEqual(response.status_code, 200)

    def test_timestamp_invalid_http_methods(self):
        """
        Test that other common HTTP request methods to the timestamp endpoint
        return status code 405 (Method Not Allowed).
        """
        # Constant for HTTP status code 405 (Method Not Allowed)
        METHOD_NOT_ALLOWED = 405

        path = reverse('timestamp:index')

        # POST
        response = self.client.post(path)
        self.assertEqual(response.status_code, METHOD_NOT_ALLOWED)

        # PATCH
        response = self.client.patch(path)
        self.assertEqual(response.status_code, METHOD_NOT_ALLOWED)

        # PUT
        response = self.client.put(path)
        self.assertEqual(response.status_code, METHOD_NOT_ALLOWED)

        # DELETE
        response = self.client.delete(path)
        self.assertEqual(response.status_code, METHOD_NOT_ALLOWED)
