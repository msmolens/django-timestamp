from django.http import JsonResponse
from django.utils import timezone
from django.views.decorators.http import require_safe


@require_safe
def index(request):
    """
    Return a JSON object containing a single "timestamp" field whose value is an
    ISO 8601-formatted string that represents the current time on this system in
    UTC.
    """
    # Get the current time on this system as a time zone-aware datetime object
    timestamp = timezone.now()

    # Create data dictionary
    data = {
        'timestamp': timestamp
    }

    # Return data in a JSON response
    return JsonResponse(data)
