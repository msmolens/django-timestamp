# Django Timestamp

[![pipeline status](https://gitlab.com/msmolens/django-timestamp/badges/master/pipeline.svg)](https://gitlab.com/msmolens/django-timestamp/commits/master)

A Django application that provides the current time in UTC.

## Author

Max Smolens (msmolens@gmail.com)

## Prerequisites

- Linux or macOS
- Python 3.7.x

Note: Tested on Ubuntu 18.04 with Python 3.7.0.

## Setup

Follow the steps below to install Django into a Python virtual environment.

### Create a virtual environment

Create a virtual environment by running:

```
$ python3 -m venv /path/to/django_env
```

Activate the virtual environment by running:

```
$ source /path/to/django_env/bin/activate
```

### Install dependencies

First, ensure that the latest version of `pip` is installed:

```
(django_env) $ python -m pip install -U pip
```

Next, install the dependencies, including Django:

```
(django_env) $ python -m pip install -r requirements.txt
```

Note that `requirements.txt` includes both runtime and development dependencies.

### Set up the database

The app currently doesn't add any models. But, for completeness, run the
following command from the root directory to set up the database:

```
(django_env) $ python mysite/manage.py migrate
```

This ensures tables are created for the built-in Admin app, for example. 

## Usage

### Run development server

To run the application using Django's development server, run:

```
(django_env) $ python mysite/manage.py runserver
```

The server should now be running at `http://127.0.0.1:8000`.

### Retrieve the current time

The application provides an endpoint (`/timestamp`) that returns the current
time in UTC. The response from this endpoint is a JSON object with a "timestamp"
field whose value is the current time on the host, in ISO 8601 format.

#### Examples

Python:

```python
import requests

response = requests.get('https://127.0.0.1:8000/timestamp')
timestamp = response.json()['timestamp']

print(timestamp)
```

This prints the timestamp, like `2018-11-14T17:26:32.409Z`, to standard output.

Note that ISO 8601 allows using either "Z" or a numerical offset like "+00:00"
as the suffix for UTC.

curl:

```bash
$ curl --silent http://127.0.0.1:8000/timestamp | python -m json.tool
{
    "timestamp": "2018-11-14T17:26:32.409Z"
}
```

## Testing

The application includes unit tests and is set up for style checking.

### Unit tests

To run the unit tests for the timestamp app, run:

```
(django_env) $ python mysite/manage.py test timestamp
```

The tests validate several aspects of the endpoint's behavior, including:

- The returned JSON data contains a `timestamp` field with a value in ISO 8601 format.
  - [JSON Schema](https://json-schema.org/) is used to codify and validate the
    expected response.
- The endpoint returns HTTP status code 200 (OK) for `GET` and `HEAD`.
- The endpoint returns HTTP status code 405 (Method Not Allowed) for other
  methods, such as `POST` and `DELETE`.

### Style checking

To perform style checking, run the following command from the root directory:

```
(django_env) $ flake8
```

## Development notes

While developing this application I recorded notes in [NOTES.md](NOTES.md),
along with links to the Django documentation.
