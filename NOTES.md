# Development Notes

This document contains notes about the steps followed when creating the Django
Timestamp application. 

## General references

- [Django documentation](https://docs.djangoproject.com/en/2.1)
- [Django tutorial](https://docs.djangoproject.com/en/2.1/intro/tutorial01/)

## Installing Django

See steps at https://docs.djangoproject.com/en/2.1/topics/install/.

Django includes a minimal web server that's suitable for testing, so there is no
need to set up a separate web server, like nginx, for example. Additionally,
Django's default configuration uses SQLite as a database, so there is no need to
set up a separate database like PostgreSQL.

## Create a Django project

Use `django-admin` to create a new Django project:

```
$ django-admin startproject mysite
```

This creates the following directory structure:

```
.
├── mysite
│   ├── manage.py
│   └── mysite
│       ├── __init__.py
│       ├── settings.py
│       ├── urls.py
│       └── wsgi.py
├── NOTES.md
└── README.md
```

References:
- https://docs.djangoproject.com/en/2.1/intro/tutorial01/#creating-a-project
- https://docs.djangoproject.com/en/2.1/ref/django-admin/#startproject

## Create a Django app

In Django, a project contains the configuration and a set of apps for a website.
App packages can be located anywhere on the module search path.

To create an app in the project, from the `mysite` directory run:

```
$ python manage.py startapp timestamp
```

This creates the following directory structure:

```
timestamp
├── __init__.py
├── admin.py
├── apps.py
├── migrations
│   └── __init__.py
├── models.py
├── tests.py
└── views.py
```

References:
- https://docs.djangoproject.com/en/2.1/intro/tutorial01/#creating-the-polls-app

## Create a view and URLconf

References:
- https://docs.djangoproject.com/en/2.1/intro/tutorial01/#write-your-first-view
- https://docs.djangoproject.com/en/2.1/topics/http/urls/

## Response objects

Django views should return `django.http.HttpResponse` objects. The
`django.http.JsonResponse` subclass sets the default `Content-Type` header to
`application/json` and accepts a `dict` to be encoded as JSON.

References:
- https://docs.djangoproject.com/en/2.1/ref/request-response/
- https://docs.djangoproject.com/en/2.1/ref/request-response/#jsonresponse-objects

## HTTP methods

Django provides decorators to restrict which HTTP methods are available to
access a view. When an unsupported method is used, the view returns an
`HttpResponseNotAllowed` object.

References:
- https://docs.djangoproject.com/en/2.1/topics/http/decorators/
- https://docs.djangoproject.com/en/2.1/ref/request-response/#django.http.HttpResponseNotAllowed

## Django time zones

Ensure `USE_TZ = True` is set in `settings.py`. This ensures that Django uses
time zone-aware datetime objects internally and stores datetime information as
UTC in the database.

Use `django.utils.timezone.now()` to create time zone-aware datetime objects.

References:
- https://docs.djangoproject.com/en/2.1/topics/i18n/timezones/

## Run the development server

In the project directory, run:

```
$ python manage.py runserver
```

By default, the server is available at `http://127.0.0.1:8000`.

References:
- https://docs.djangoproject.com/en/2.1/intro/tutorial01/#the-development-server
- https://docs.djangoproject.com/en/2.1/ref/django-admin/#runserver

## Testing

References:
- https://docs.djangoproject.com/en/2.1/intro/tutorial05/#test-a-view
- https://docs.djangoproject.com/en/2.1/topics/testing/
- https://docs.djangoproject.com/en/2.1/topics/testing/tools/
